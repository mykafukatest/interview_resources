// 定义一个名为 button-counter 的新组件
Vue.component('head-navigate', {
    data: function () {
        return {
            userInfo:{},
        }
    },
    created(){
        axios.post("/auth/isLogin?token=" + localStorage.getItem('token')).then(response => {
            if(response.data.code!==20000){
                localStorage.removeItem("userInfo")
            }else {
                this.userInfo= JSON.parse(localStorage.getItem("userInfo"))
            }
        }).catch(error=>{
            this.$message({
                message: "服务器异常！",
                type: 'error'
            });

        });
    },
    updated(){

    },
    mounted(){
    },
    methods:{
    },
    template: '<header class="gird-header">\n' +
        '        <div class="header-fixed">\n' +
        '            <div class="header-inner">\n' +
        '                <a href="javascript:void(0)" class="header-logo" id="logo" >{{userInfo.username!=null?userInfo.username:"feng"}}</a>\n' +
        '                <nav class="nav" id="nav">\n' +
        '                    <ul>\n' +
        '                        <li><a href="write.html">写博客</a></li>\n' +
        '                        <li><a href="article.html">博客</a></li>\n' +
        '                        <li><a href="message.html">留言</a></li>\n' +
        '                        <li><a href="link.html">友链</a></li>\n' +
        '                        <li><a href="link.html">友链</a></li>\n' +
        '                    </ul>\n' +
        '                </nav>\n' +
        '                <a href="login.html" class="blog-user">\n' +
        '                    <i v-if="userInfo.avatar==null" class="fa fa-qq"></i>\n' +
        '                </a>\n' +
        '                <a href="javascript:voide(0)" class="blog-user">\n' +
        '                    <img v-if="userInfo.avatar!=null" class="fa" :src="userInfo.avatar"/>\n' +
        '                </a>\n' +
        '                <a class="phone-menu">\n' +
        '                    <i></i>\n' +
        '                    <i></i>\n' +
        '                    <i></i>\n' +
        '                </a>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </header>'

})