package com.lbl.common.mapper;

import com.lbl.common.entity.CodeLevel;
import tk.mybatis.mapper.common.Mapper;

public interface CodeLevelMapper extends Mapper<CodeLevel> {

}
