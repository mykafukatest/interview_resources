package com.lbl.common.mapper;

import com.lbl.common.entity.User;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface UserMapper extends Mapper<User> {
    int updateBatch(List<User> list);

    int batchInsert(@Param("list") List<User> list);

    int insertOrUpdate(User record);

    int insertOrUpdateSelective(User record);


    List<User> findFillInUser(@Param("size") int size,@Param("usernames") List<String> usernames);
}