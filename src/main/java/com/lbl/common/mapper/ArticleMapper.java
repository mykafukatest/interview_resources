package com.lbl.common.mapper;

import com.lbl.common.entity.Article;
import tk.mybatis.mapper.common.Mapper;

public interface ArticleMapper extends Mapper<Article> {

}
