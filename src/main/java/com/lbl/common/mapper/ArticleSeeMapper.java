package com.lbl.common.mapper;

import com.lbl.admin.vo.ArticleSeeVO;
import com.lbl.common.entity.ArticleSee;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @author : TBH
 * @date : 2020-11-05-13:57
 */
public interface ArticleSeeMapper extends Mapper<ArticleSee> {
    List<ArticleSeeVO> findRecent(@Param("page") int page, @Param("size") int size);

    Long findIdByUid(Long uid);
}
