package com.lbl.common.mapper;

import com.lbl.common.entity.UserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<UserRole> {

}
