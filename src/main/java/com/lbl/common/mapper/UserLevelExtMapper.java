package com.lbl.common.mapper;

import com.lbl.common.entity.UserLevelExt;
import tk.mybatis.mapper.common.Mapper;

public interface UserLevelExtMapper extends Mapper<UserLevelExt> {

}
