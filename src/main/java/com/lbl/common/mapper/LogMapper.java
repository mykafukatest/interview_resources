package com.lbl.common.mapper;

import com.lbl.common.entity.Log;
import tk.mybatis.mapper.common.Mapper;

public interface LogMapper extends Mapper<Log> {

}
