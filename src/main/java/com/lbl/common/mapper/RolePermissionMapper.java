package com.lbl.common.mapper;

import com.lbl.common.entity.RolePermission;
import tk.mybatis.mapper.common.Mapper;

public interface RolePermissionMapper extends Mapper<RolePermission> {

}
