package com.lbl.common.mapper;

import com.lbl.common.entity.ArticleType;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author : TBH
 * @date : 2020-11-05-13:57
 */
public interface ArticleTypeMapper extends Mapper<ArticleType> {
}
