package com.lbl.common.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @ClassName QueryP83RuleReturnBean
 * @Description: 查询高义爬虫外部账套集合
 * @Author: lzm
 * @CreateDate: 2020/04/26 15:32
 * @Version: 1.0
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QueryP83RuleReturnBean implements Serializable {

    private static final long serialVersionUID = 89230985921229L;

    /**
     * 账套名称
     */
    private String segName;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 1:区域代理 2:门口协议
     */
    private String type;

    /**
     * 收货地
     */
    private String placeOfReceipt;

    /**
     * 收货详细地址
     */
    private String detailedAddress;

    /**
     * 委托人
     */
    private String entrustPerson;

    /**
     * 委托人电话
     */
    private String entrustPhone;

    /**
     * 收货人
     */
    private String receiverPerson;

    /**
     * 收货人电话
     */
    private String receiverPhone;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 路损类型
     */
    private String roadLossType;

    /**
     * 备注
     */
    private String remark;
}
