package com.lbl.common.utils;

import cn.hutool.json.JSONUtil;
import com.beust.jcommander.internal.Maps;
import com.lbl.common.entity.Log;
import com.lbl.admin.security.UserDetailsVo;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * @author lbl
 * @date 2020/6/28 10:54
 */
public class LogUtils {

    public static Log createLogInfo(HttpServletRequest request, Authentication authentication,String module,String method){
        Log log=new Log();
        log.setModule(module);
        log.setMethod(method);
        log.setDescription("success");
        log.setStatus("0");
        log.setIp(request.getRemoteAddr());
        log.setUrl(request.getRequestURI());
        log.setCreate(new Date());
        UserDetailsVo detailsVo = (UserDetailsVo) authentication.getPrincipal();
        Map<String, Object> params = Maps.newHashMap();
        params.put("username",detailsVo.getUsername());
        log.setParams(JSONUtil.toJsonStr(params));
        log.setNickname(detailsVo.getName());
        log.setUsername(detailsVo.getUsername());
        log.setAction("0ms");
        return log;
    }
}
