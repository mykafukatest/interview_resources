package com.lbl.common.utils;

import cn.hutool.json.JSONUtil;
import org.apache.commons.codec.binary.Base64;

import java.security.MessageDigest;

public class MD5Utils {

	/**
	 * @Description: 对字符串进行md5加密 
	 */
	public static String getMD5Str(String strValue) throws Exception {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		String newstr = Base64.encodeBase64String(md5.digest(strValue.getBytes()));
		return newstr;
	}

	public static void main(String[] args) {
		String json="{'placeOfReceipt':'上海市','detailedAddress':'上海市宝山区漠河路300号','entrustPerson':'宝山区漠河路300号','entrustPhone':'15000236654','receiverPerson':'赵六','receiverPhone':'15866321542','price':'100','roadLossType':'测试'}";
        QueryP83RuleReturnBean returnBean = JSONUtil.toBean(json, QueryP83RuleReturnBean.class);
        System.out.println(returnBean);
    }
}
