package com.lbl.common.utils;

import java.util.UUID;

public class UUIDutils {
	public static String generateId() {
		
		return generateId(32);
	}
	public static String generateId(int wei) {
		StringBuffer buffer=new StringBuffer();
		if(wei==32) {
			buffer.append(UUID.randomUUID().toString().replaceAll("-", ""));
		}else {
			buffer.append(UUID.randomUUID().toString().replaceAll("-", "")).append(UUID.randomUUID().toString().replaceAll("-", ""));
		}
		return buffer.toString();
	}
	public static void main(String[] args) {
		System.out.println(generateId());
	}
}	
