package com.lbl.common.entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * userLevelExt实体类
 * @author Administrator
 *
 */
@Table(name="tb_user_level_ext")
public class UserLevelExt implements Serializable{

	/**
 	 * 主键
	*/
	@Id
	private Integer id;


	
	/**
 	 * 账号
	*/
	private String username;
	/**
 	 * 经验
	*/
	private Integer exp;

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getExp() {
		return exp;
	}
	public void setExp(Integer exp) {
		this.exp = exp;
	}


	
}
