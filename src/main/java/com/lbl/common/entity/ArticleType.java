package com.lbl.common.entity;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * article实体类
 * @author Administrator
 *
 */
@Table(name="tb_article_type")
@Data
public class ArticleType implements Serializable{

	/**
 	 * 主键
	*/
	@Id
	private Integer id;
	/**
 	 * 类型名称
	*/
	private String typeName;
	/**
 	 * 类型代码
	*/
	private String typeCode;

}
