package com.lbl.common.entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * codeLevel实体类
 * @author Administrator
 *
 */
@Table(name="tb_code_level")
public class CodeLevel implements Serializable{

	/**
 	 * 主键
	*/
	@Id
	private Integer id;


	
	/**
 	 * 等级名称
	*/
	private String name;
	/**
 	 * 等价标记
	*/
	private String code;
	/**
 	 * 经验阈值
	*/
	private Integer experience;

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public Integer getExperience() {
		return experience;
	}
	public void setExperience(Integer experience) {
		this.experience = experience;
	}


	
}
