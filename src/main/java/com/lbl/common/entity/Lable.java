package com.lbl.common.entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * lable实体类
 * @author Administrator
 *
 */
@Table(name="tb_lable")
public class Lable implements Serializable{

	/**
 	 * 主键
	*/
	@Id
	private Integer id;


	
	/**
 	 * 分类标签
	*/
	private String lableName;
	/**
 	 * 分类代码
	*/
	private String lableCode;
	/**
 	 * 创建时间
	*/
	private java.util.Date createTime;
	/**
 	 * 排序（越大越往前）
	*/
	private Integer sortCode;

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getLableName() {
		return lableName;
	}
	public void setLableName(String lableName) {
		this.lableName = lableName;
	}

	public String getLableCode() {
		return lableCode;
	}
	public void setLableCode(String lableCode) {
		this.lableCode = lableCode;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	public Integer getSortCode() {
		return sortCode;
	}
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}


	
}
