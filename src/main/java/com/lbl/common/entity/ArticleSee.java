package com.lbl.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author : TBH
 * @date : 2020-11-05-13:54
 * 最近访客实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "tb_article_see")
public class ArticleSee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * 访问的用户id
     */
    @Column(name = "uid")
    private Long uid;

    /**
     * 访问的时间
     */
    @Column(name = "see_time")
    private Date seeTime;
}
