package com.lbl.common.entity;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * article实体类
 * @author Administrator
 *
 */
@Table(name="tb_article")
public class Article implements Serializable{

	/**
 	 * 主键
	*/
	@Id
	private Integer id;


	
	/**
 	 * 标题
	*/
	private String articleTitle;
	/**
 	 * 标签
	*/
	private String lableCode;
	/**
 	 * 观看数
	*/
	private Integer views;
	/**
 	 * 点赞数
	*/
	private Integer likes;
	/**
 	 * 评论数
	*/
	private Integer comments;
	/**
 	 * 封面图
	*/
	private String articleImg;
	/**
 	 * 内容
	*/
	private String articleContent;
	/**
 	 * 发布时间
	*/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private java.util.Date createTime;
	/**
 	 * 最后评论时间
	*/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private java.util.Date lastCommentsTime;
	/**
 	 * 排序
	*/
	private Integer sortCode;
	/**
 	 * 是否顶置1是0否
	*/
	private Integer isTop;

	/**
	 * 类别
	 */
	private String typeCode;

	/**
	 * 状态
	 */
	private Integer status;

	/**
	 * 用户
	 */
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getArticleTitle() {
		return articleTitle;
	}
	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}

	public String getLableCode() {
		return lableCode;
	}
	public void setLableCode(String lableCode) {
		this.lableCode = lableCode;
	}

	public Integer getViews() {
		return views;
	}
	public void setViews(Integer views) {
		this.views = views;
	}

	public Integer getLikes() {
		return likes;
	}
	public void setLikes(Integer likes) {
		this.likes = likes;
	}

	public Integer getComments() {
		return comments;
	}
	public void setComments(Integer comments) {
		this.comments = comments;
	}

	public String getArticleImg() {
		return articleImg;
	}
	public void setArticleImg(String articleImg) {
		this.articleImg = articleImg;
	}

	public String getArticleContent() {
		return articleContent;
	}
	public void setArticleContent(String articleContent) {
		this.articleContent = articleContent;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	public java.util.Date getLastCommentsTime() {
		return lastCommentsTime;
	}
	public void setLastCommentsTime(java.util.Date lastCommentsTime) {
		this.lastCommentsTime = lastCommentsTime;
	}

	public Integer getSortCode() {
		return sortCode;
	}
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}

	public Integer getIsTop() {
		return isTop;
	}
	public void setIsTop(Integer isTop) {
		this.isTop = isTop;
	}


	
}
