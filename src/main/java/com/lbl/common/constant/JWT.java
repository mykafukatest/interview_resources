package com.lbl.common.constant;

import com.beust.jcommander.internal.Lists;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author lbl
 * @date 2019/10/22 11:15
 */
@Component
public class JWT implements InitializingBean {

    @Value("${ignore.equal.url}")
    private String equalUrl;
    @Value("${ignore.fuzzy.url}")
    private String fuzzyUrl;
    @Value("${ignore.suffix.url}")
    private String suffixUrl;

    private List<String> EQUIVALENT_IGNORING_URL= Lists.newArrayList();
    private List<String> FUZZY_IGNORING_URL= Lists.newArrayList();
    private List<String> SUFFIX_IGNORING_URL = Lists.newArrayList();


    public static String CLAIM_KEY_USERNAME = "sub";
    public static String CLAIM_KEY_ID = "id";
    public static String CLAIM_KEY_NAME = "name";
    public static  String CLAIM_KEY_CREATED = "created";
    public static  String CLAIM_KEY_ROLES = "roles";
    /**
     * 所有忽略的url
     * @return
     */
    public List<String> ignoringUrlAll(){
        EQUIVALENT_IGNORING_URL.addAll(FUZZY_IGNORING_URL);
        EQUIVALENT_IGNORING_URL.addAll(SUFFIX_IGNORING_URL);
        return EQUIVALENT_IGNORING_URL;
    }

    /**
     * 模糊匹配url /**
     * @return
     */
    public List<String> ignoringUrlFuzzyMatching(){
        return FUZZY_IGNORING_URL.stream().map(e->e.substring(0,e.lastIndexOf("/**"))).collect(Collectors.toList());
    }

    /**
     * 等值匹配url
     * @return
     */
    public List<String> ignoringUrlPrecision() {
        return EQUIVALENT_IGNORING_URL;
    }

    /**
     * 后缀匹配url *.xxx
     * @return
     */
    public List<String> ignoringSuffixMatchPrecision(){
        return SUFFIX_IGNORING_URL.stream().map(e->e.substring(e.lastIndexOf("*.")+2)).collect(Collectors.toList());
    }

    /**
     * 后缀匹配url *.xxx
     * @return
     */
    List<String> ignoringSuffixScMatchPrecision(){
        return SUFFIX_IGNORING_URL;
    }

    @Override
    public void afterPropertiesSet(){
        if(!StringUtils.isEmpty(equalUrl)){
            EQUIVALENT_IGNORING_URL=Stream.of(equalUrl.split(",")).collect(Collectors.toList());
        }
        if(!StringUtils.isEmpty(fuzzyUrl)){
            FUZZY_IGNORING_URL=Stream.of(fuzzyUrl.split(",")).collect(Collectors.toList());
        }
        if(!StringUtils.isEmpty(suffixUrl)){
            SUFFIX_IGNORING_URL=Stream.of(suffixUrl.split(",")).collect(Collectors.toList());
        }
    }
}
