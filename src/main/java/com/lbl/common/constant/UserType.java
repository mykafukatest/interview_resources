package com.lbl.common.constant;

/**
 * @author : FallenRunning (汤北寒)
 * @date : 2019/10/21/16:53
 * @desc : 用户注册类型
 */
public interface UserType {
    Integer QQ = 0 ;
    Integer WECHAT = 1;
    Integer NORMAL = 2;
}
