package com.lbl.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author lbl
 * @date 2020/8/6 8:57
 */
@Configuration
public class ThreadPoolConfig {


    @Bean
    public ExecutorService logThreadPoolExecutor(){
        return Executors.newFixedThreadPool(20);
    }
}
