package com.lbl.common.aspects;

import cn.hutool.json.JSONUtil;
import com.beust.jcommander.internal.Maps;
import com.lbl.common.annotation.SystemLog;
import com.lbl.common.entity.Log;
import com.lbl.common.mapper.LogMapper;
import com.lbl.admin.service.UserInfoConvert;
import com.lbl.admin.vo.UserDTO;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ExecutorService;

@Component
@Aspect
public class SystemLogAspect {
    @Autowired
    private ExecutorService logThreadPoolExecutor;
    @Autowired
    private UserInfoConvert userInfoConvert;
    @Autowired
    private  LogMapper logMapper;

    @Pointcut("@annotation(com.lbl.common.annotation.SystemLog)")
    private void pointCut() {
    }

    @Around("pointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        long beginTime=0,endTime = 0;
        // 日志实体对象
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))
                .getRequest();
        // 拦截的实体类，就是当前正在执行的controller
        Object target = point.getTarget();
        // 拦截的方法名称。当前正在执行的方法
        String methodName = point.getSignature().getName();
        // 拦截的方法参数
        // 拦截的放参数类型
        Signature sig = point.getSignature();
        MethodSignature msig = null;
        msig = (MethodSignature) sig;
        Object[] args = point.getArgs();
        String[] parameterNames = msig.getParameterNames();
        List<String> parameterNamesFilter=new ArrayList<>();
        List<Object> argsFilter=new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            if(!(args[i] instanceof ServletRequest ||args[i] instanceof ServletResponse)){
                parameterNamesFilter.add(parameterNames[i]);
                argsFilter.add(args[i]);
            }
        }
        Map<String, Object> params = Maps.newHashMap();
        for (int i = 0; i < parameterNames.length; i++) {
            params.put(parameterNamesFilter.get(i),argsFilter.get(i));
        }
        Class[] parameterTypes = msig.getMethod().getParameterTypes();
        Object object = null;
        Method method = null;
        try {
            method = target.getClass().getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException | SecurityException e1) {
            e1.printStackTrace();
        }
        Log log = new Log();

        SystemLog systemlog = method.getAnnotation(SystemLog.class);
        log.setModule(systemlog.module());
        log.setMethod(systemlog.method());
        log.setIp(request.getRemoteAddr());
        log.setUrl(request.getRequestURI());
        log.setParams(JSONUtil.toJsonStr(params));
        try {
            beginTime = System.currentTimeMillis();
            object = point.proceed();
            endTime = System.currentTimeMillis();
            log.setDescription("执行成功");
            log.setStatus("1");
        } catch (Throwable e) {
            log.setDescription(String.format("执行失败:%s",e.getMessage()));
            log.setStatus("0");
            endTime = System.currentTimeMillis();
        }
        log.setAction(((endTime- beginTime)) + "ms");
        UserDTO userDTO = userInfoConvert.jwtUserToUserDTO();
        if(userDTO != null){
          logThreadPoolExecutor.execute(()->{
              log.setCreate(new Date());
              log.setUsername(userDTO.getUsername());
              log.setNickname( userDTO.getNickname());
              logMapper.insert(log);
          });
        }
        return object;
    }
}