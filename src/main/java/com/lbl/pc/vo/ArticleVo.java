package com.lbl.pc.vo;

import com.lbl.common.entity.Article;
import com.lbl.common.entity.ArticleType;
import com.lbl.common.entity.Lable;
import com.lbl.common.entity.User;

public class ArticleVo extends Article {
    private Lable lablel;
    private ArticleType articleType;
    private User user;

    public Lable getLablel() {
        return lablel;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setLablel(Lable lablel) {
        this.lablel = lablel;
    }

    public ArticleType getArticleType() {
        return articleType;
    }

    public void setArticleType(ArticleType articleType) {
        this.articleType = articleType;
    }
}
