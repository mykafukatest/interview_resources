package com.lbl.pc.controller;

import cn.hutool.core.date.DateUtil;
import com.lbl.admin.service.UserService;
import com.lbl.common.entity.MailSend;
import com.lbl.common.entity.User;
import com.lbl.common.utils.ResponseResult;
import com.lbl.common.utils.SendMailUtils;
import com.lbl.common.utils.VerifyCodeUtils;
import com.sun.org.apache.regexp.internal.RE;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Encoder;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/pcUser")
@Api(tags = "pc前端用户调用接口")
public class PcUserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private SendMailUtils sendMailUtils;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Value("${activeCode.redis.prefix}")
    private String KEYPREFIX;

    @Value("${verification.redis.prefix}")
    private String VERIFICATION_KEYPREFIX;
    /**
     * 生成验证码
     * @param session
     * @param response
     * @throws IOException
     */
    @GetMapping("/verification")
    public ResponseResult verification(HttpSession session, HttpServletResponse response,@RequestParam("key")String key) throws IOException {
        //生成随机字串
        String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
        redisTemplate.opsForValue().set(VERIFICATION_KEYPREFIX+key,verifyCode,3, TimeUnit.MINUTES);
        System.out.println(session.getId());
        //输出到图片
        ByteArrayOutputStream data = new ByteArrayOutputStream();
        VerifyCodeUtils.outputImage(100,40,data,verifyCode);
        //把图片转base64返回
        BASE64Encoder encoder = new BASE64Encoder();
        return ResponseResult.success("data:image/png;base64,"+encoder.encode(data.toByteArray()));
    }

    /**
     * 发送激活码
     * @param username
     * @throws IOException
     */
    @PostMapping("/sendMail")
    public ResponseResult sendMail(@RequestParam("username")String username) throws IOException {

        String activeCode = VerifyCodeUtils.generateVerifyCode(6,VerifyCodeUtils.MAIL_CODES);
        MailSend mailSend = new MailSend();
        String content = String.format("账号:%s的注册激活码 %s 提示：该邮件30分钟内有效！发送时间:%s", username, activeCode, DateUtil.format(new Date(), "yyyy-MM-dd HH:mm"));
        mailSend.setContent(content);
        mailSend.setTopic("来自lzmy.top的激活码");
        mailSend.setRecipient(username);
        sendMailUtils.sendSimpleMail(mailSend);
        redisTemplate.opsForValue().set(KEYPREFIX+username,activeCode,30, TimeUnit.MINUTES);

        return ResponseResult.success();
    }
    /**
     * 账号是否被注册 true 已经注册 false 没有注册
     * @param username
     * @throws IOException
     */
    @GetMapping("/isAccountRegister")
    public ResponseResult isAccountRegister(@RequestParam("username")String username) throws IOException {

        return ResponseResult.success(userService.isAccountRegister(username));
    }

    /**
     * 注册账号
     * @param
     * @throws IOException
     */
    @PostMapping("/register")
    public ResponseResult register(@RequestBody User user,
                                   @RequestParam("activeCode")String activeCode,
                                   @RequestParam("code")String code,
                                   HttpSession session) throws IOException {
        String redisCode = redisTemplate.opsForValue().get(session.getId());
        if(StringUtils.isEmpty(redisCode)){
            return ResponseResult.error("验证码已过期！");
        }
        if(!Objects.equals(redisCode.toUpperCase(),code.trim().toUpperCase())){
            return ResponseResult.error("验证码错误！");
        }
        String redisActiveCode = redisTemplate.opsForValue().get(KEYPREFIX+user.getUsername());
        if(!Objects.equals(activeCode.trim(),redisActiveCode)){
            return ResponseResult.error("激活码不正确！");
        }
        user.setCreated(new Date());
        user.setUpdated(new Date());
        user.setEmail(user.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userService.add(user);
        redisTemplate.delete(KEYPREFIX+user.getUsername());
        return ResponseResult.success();
    }

}
