package com.lbl.pc.controller;

import com.lbl.common.entity.Article;
import com.lbl.common.utils.PageResult;
import com.lbl.common.utils.ResponseResult;
import com.lbl.common.utils.VerifyCodeUtils;
import com.lbl.pc.service.ArticleService;
import com.lbl.pc.vo.ArticleVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/pcArticle")
@Api(tags = "pc前端文章调用接口")
public class PcArticleController {

    @Autowired
    private ArticleService articleService;

    /**
     * 获取文章
     * @param page page
     * @param size size
     * @throws IOException
     */
    @GetMapping("/query/{page}/{size}")
    public PageResult<ArticleVo> queryArticles(@PathVariable("page")Integer page, @PathVariable("size")Integer size, @RequestParam(value = "lable",required = false)String lable) throws IOException {
        Map<String, Object> searchMap = new HashMap<>();
        searchMap.put("status","1");
        searchMap.put("lableCode",lable);
        return articleService.queryArticleByPage(page, size, searchMap);
    }

    /**
     * 获取热门文章
     * @param page page
     * @param size size
     * @throws IOException
     */
    @GetMapping("/queryPopularArticles/{page}/{size}")
    public PageResult<ArticleVo> queryPopularArticles(@PathVariable("page")Integer page, @PathVariable("size")Integer size) throws IOException {
        Map<String, Object> searchMap = new HashMap<>();
        searchMap.put("status","1");
        searchMap.put("sort","views desc,is_top desc,sort_code desc,create_time desc");
        return articleService.queryArticleByPage(page, size, searchMap);
    }

    /**
     * 获取置顶文章
     * @param page page
     * @param size size
     * @throws IOException
     */
    @GetMapping("/queryTopArticles/{page}/{size}")
    public PageResult<ArticleVo> queryTopArticles(@PathVariable("page")Integer page, @PathVariable("size")Integer size) throws IOException {
        Map<String, Object> searchMap = new HashMap<>();
        searchMap.put("status","1");
        searchMap.put("sort","is_top desc,sort_code desc,create_time desc");
        return articleService.queryArticleByPage(page, size, searchMap);
    }

    /**
     * 获取根据id获取文章
     * @param id
     * @return
     * @throws IOException
     */
    @GetMapping("/getArticle/{id}")
    public ArticleVo getArticle(@PathVariable("id")Integer id) throws IOException {

        ArticleVo articleVo = articleService.getArticle(id);
        Article article=new Article();
        article.setId(articleVo.getId());
        article.setViews(articleVo.getViews()+1);
        articleService.updateArticle(article);
        articleVo.setViews(articleVo.getViews()+1);
        return articleVo;
    }



}
