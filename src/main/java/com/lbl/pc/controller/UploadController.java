package com.lbl.pc.controller;

import com.lbl.common.constant.UploadConstant;
import com.lbl.common.utils.FastDFSClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private FastDFSClientUtil fastDFSClientUtil;

    @PostMapping("/native")
    public String nativeUpload(@RequestParam("file") MultipartFile file) {
        long currentTimeMillis=System.currentTimeMillis();
        String filePath = UploadConstant.nativeUploadImgPath +"\\"+currentTimeMillis%20+"\\"+currentTimeMillis+"\\"+ file.getOriginalFilename();
        File desFile = new File(filePath);
        if(!desFile.exists()){
            desFile.mkdirs();
        }
        try {
            file.transferTo(desFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("path:---"+filePath);
        return "http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"\\"+currentTimeMillis%20+"\\"+currentTimeMillis+"\\"+file.getOriginalFilename();
    }

    @PostMapping("/serverUpload")
    public String serverUpload(@RequestParam("file") MultipartFile file) throws IOException {
        return fastDFSClientUtil.uploadFile(file);
    }

}