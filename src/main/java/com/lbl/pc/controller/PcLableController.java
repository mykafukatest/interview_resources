package com.lbl.pc.controller;

import com.lbl.common.entity.Lable;
import com.lbl.common.utils.PageResult;
import com.lbl.pc.service.ArticleService;
import com.lbl.pc.service.LableService;
import com.lbl.pc.vo.ArticleVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pcLable")
@Api(tags = "pc前端标签调用接口")
public class PcLableController {

    @Autowired
    private LableService lableService;

    /**
     * 获取标签
     * @param page page
     * @param size size
     * @throws IOException
     */
    @GetMapping("/queryLable/{page}/{size}")
    public PageResult<Lable> queryLable(@PathVariable("page")Integer page, @PathVariable("size")Integer size) throws IOException {
        Map<String, Object> searchMap = new HashMap<>();
        return lableService.queryLableByPage(page, size, searchMap);
    }

}
