package com.lbl.pc.controller;

import com.lbl.admin.service.UserService;
import com.lbl.admin.vo.ArticleSeeVO;
import com.lbl.admin.vo.UserVo;
import com.lbl.common.entity.ArticleSee;
import com.lbl.common.entity.User;
import com.lbl.common.utils.JwtTokenUtils;
import com.lbl.common.utils.ResponseResult;
import com.lbl.pc.service.ArticleSeeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : TBH
 * @date : 2020-11-05-13:58
 */

@RestController
@RequestMapping("/pcArticleSee")
@Api(tags = "pc前端文章最近访客接口")
public class PcArticleSeeController {

    @Resource(name = "articleSeeService")
    private ArticleSeeService articleSeeService;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenUtils jwtTokenUtils;

    /**
     * 添加文章最近访客
     * @param token 请求的token
     * @return
     */
    @PostMapping("/add")
    public ResponseResult<Void> addArticleSee(@RequestParam("token") String token){
        String username = jwtTokenUtils.getUsernameFromToken(token);
        UserVo user = userService.findUserByUserName(username);
        //创建对象,使用Service存下来
        ArticleSee articleSee = new ArticleSee();
        articleSee.setUid(user.getId());
        articleSee.setSeeTime(new Date());
        articleSeeService.save(articleSee);
        return ResponseResult.success();
    }

    /**
     *  查询最近访客 默认查询12个
     * @param size 每页大小
     * @param page 第几页
     * @return
     */
    @GetMapping("/find")
    public ResponseResult findRecent(@RequestParam(value = "size",required = false,defaultValue = "12") int size,@RequestParam(value = "page",required = false,defaultValue = "0") int page){
        List<ArticleSeeVO> articleSeeVOS = articleSeeService.findRecent(page,size);
        List<String> usernames = articleSeeVOS.stream().map(ArticleSeeVO::getUsername).collect(Collectors.toList());
        if(articleSeeVOS.size() < size){
            //少于12个从tb_user里面拿
            List<User> users = userService.findFillInUser(size - articleSeeVOS.size(),usernames);
            users.forEach( user -> {
                ArticleSeeVO articleSeeVO = new ArticleSeeVO();
                articleSeeVO.setAvatar(user.getAvatar());
                articleSeeVO.setUsername(user.getUsername());
                articleSeeVO.setName(user.getName());

                articleSeeVOS.add(articleSeeVO);

            });
        }
        return ResponseResult.success(articleSeeVOS);
    }
}
