package com.lbl.pc.controller;

import com.lbl.common.entity.ArticleType;
import com.lbl.common.utils.PageResult;
import com.lbl.pc.service.ArticleService;
import com.lbl.pc.service.ArticleTypeService;
import com.lbl.pc.vo.ArticleVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pcArticleType")
@Api(tags = "pc前端类型调用接口")
public class PcArticleTypeController {

    @Autowired
    private ArticleTypeService articleTypeService;

    /**
     * 获取文章类型
     * @param page page
     * @param size size
     * @throws IOException
     */
    @GetMapping("/query/{page}/{size}")
    public PageResult<ArticleType> queryArticles(@PathVariable("page")Integer page, @PathVariable("size")Integer size) throws IOException {
        return articleTypeService.findPage(page, size);
    }

}
