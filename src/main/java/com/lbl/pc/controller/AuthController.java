package com.lbl.pc.controller;

import com.lbl.admin.service.UserService;
import com.lbl.common.entity.Article;
import com.lbl.common.entity.User;
import com.lbl.common.utils.ResponseResult;
import com.lbl.pc.service.ArticleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private ArticleService articleService;

    @RequestMapping("/isLogin")
    public ResponseResult<String> isLogin() {
        return ResponseResult.success();
    }

    @RequestMapping("/getUserInfo")
    public ResponseResult<String> getUserInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        User user = userService.findUserInfoByUsername(name);
        user.setPassword(null);
        return ResponseResult.success(user);
    }

    /**
     * 发表文章
     * @param article
     * @return
     * @throws IOException
     */
    @PostMapping("/saveArticle")
    public ResponseResult saveArticle(@RequestBody Article article) throws IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        article.setUsername(authentication.getName());
        articleService.saveArticle(article);
        return ResponseResult.success();
    }
}

