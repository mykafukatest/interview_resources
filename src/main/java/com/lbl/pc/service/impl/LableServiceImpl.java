package com.lbl.pc.service.impl;
import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageInfo;
import com.lbl.common.entity.Article;
import com.lbl.common.entity.Lable;
import com.lbl.common.mapper.LableMapper;
import com.lbl.common.utils.PageResult;
import com.lbl.pc.vo.ArticleVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lbl.pc.service.LableService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class LableServiceImpl implements LableService {

    @Autowired
    private LableMapper lableMapper;


    @Override
    public PageResult<Lable> queryLableByPage(Integer page, Integer size, Map<String, Object> rules) {
        PageHelper.startPage(page,size,rules.getOrDefault("sort","sort_code desc,create_time desc").toString());

        PageInfo<Lable> pageInfo=new PageInfo<>(lableMapper.selectByExample(this.createExample(rules)));
        return new PageResult<>(pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Lable.class);
        Example.Criteria criteria = example.createCriteria();
        if(CollUtil.isNotEmpty(searchMap)){
            //
        }
        return example;
    }
}
