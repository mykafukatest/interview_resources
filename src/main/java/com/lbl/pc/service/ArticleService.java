package com.lbl.pc.service;
import com.github.pagehelper.PageInfo;
import com.lbl.common.entity.Article;
import com.lbl.common.utils.PageResult;
import com.lbl.pc.vo.ArticleVo;

import java.util.*;

/**
 * article业务逻辑层
 */
public interface ArticleService {

    PageResult<ArticleVo> queryArticleByPage(Integer page, Integer size, Map<String,Object> rules);

    int saveArticle(Article article);

    ArticleVo getArticle(Integer id);

    int updateArticle(Article article);

}
