package com.lbl.pc.service;

import com.github.pagehelper.PageInfo;
import com.lbl.common.entity.ArticleType;
import com.lbl.common.utils.PageResult;
import com.lbl.pc.vo.ArticleVo;

/**
 * lable业务逻辑层
 */
public interface ArticleTypeService {
    public PageResult<ArticleType> findPage(int page, int size);
}
