package com.lbl.pc.service;
import com.github.pagehelper.PageInfo;
import com.lbl.common.entity.Lable;
import com.lbl.common.utils.PageResult;
import com.lbl.pc.vo.ArticleVo;

import java.util.*;

/**
 * lable业务逻辑层
 */
public interface LableService {
    PageResult<Lable> queryLableByPage(Integer page, Integer size, Map<String,Object> rules);

}
