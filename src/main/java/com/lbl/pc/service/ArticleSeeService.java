package com.lbl.pc.service;

import com.lbl.admin.vo.ArticleSeeVO;
import com.lbl.common.entity.ArticleSee;

import java.util.List;

/**
 * @author : TBH
 * @date : 2020-11-05-13:57
 */
public interface ArticleSeeService {
    void save(ArticleSee articleSee);

    List<ArticleSeeVO> findRecent(int page,int size);
}
