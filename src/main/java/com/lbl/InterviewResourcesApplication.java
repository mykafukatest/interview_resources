package com.lbl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;


@SpringBootApplication
@MapperScan("com.lbl.common.mapper")
public class InterviewResourcesApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterviewResourcesApplication.class, args);
    }




}