package com.lbl.admin.security;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Date;

/**
 * @author lbl
 * @date 2020/5/15 10:29
 */
@Data
public class UserDetailsVo extends User {
    private Long id;

    private String phone;
    /**
     * 注册邮箱
     */
    private String email;

    private Date created;

    private String name;

    private Date updated;

    public UserDetailsVo(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }
}
