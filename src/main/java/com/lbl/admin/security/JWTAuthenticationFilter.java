package com.lbl.admin.security;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.lbl.common.constant.JWT;
import com.lbl.common.utils.JwtResult;
import com.lbl.common.utils.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author lbl
 * @date 2019/10/16 20:28
 */
@Slf4j
@Component
public class JWTAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtTokenUtils jwtTokenUtils;

    @Autowired
    private JWT jwt;

    //解析 jwt  将用户信息存入SecurityContextHolder中
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        //登录页面放行
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        String uri = request.getRequestURI();
        String uriStart=uri.substring(0,getCharacterPosition(uri,'/',2));
        String uriEnd=uri.substring(uri.lastIndexOf(".")+1);
        if(jwt.ignoringUrlPrecision().contains(uri)
                || jwt.ignoringUrlFuzzyMatching().stream().anyMatch(e-> Objects.equals(uriStart,e))
                || jwt.ignoringSuffixMatchPrecision().stream().anyMatch(e-> Objects.equals(uriEnd,e))){
            chain.doFilter(request, response);
            return;
        }
        //
        String access_token = request.getHeader("X-Token");
        if(StringUtils.isEmpty(access_token)){
            access_token = request.getParameter("token");
        }
        if (StringUtils.isEmpty(access_token)) {
            response.getWriter().print(JSONUtil.toJsonStr(JwtResult.error("请携带token")));
            response.getWriter().flush();
            response.getWriter().close();
            return;
        }
        if(jwtTokenUtils.isTokenExpired(access_token)){
            String msg = JSONUtil.toJsonStr(JwtResult.invalid("access_token 已经失效"));
            log.info("******access_token 已经过期:{}",msg);
            response.getWriter().print(msg);
            response.getWriter().flush();
            response.getWriter().close();
            return;
        }
        UsernamePasswordAuthenticationToken authentication = jwtTokenUtils.getAuthentication(access_token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    /**
     * 查找 Key 第 n次出现的位置
     * @param string
     * @param searchKey
     * @param n
     * @return
     */
    public int getCharacterPosition(String string,char searchKey,Integer n){
        char[] chars = string.toCharArray();
        int position=-1,count=0;
        for (int i = 0; i < chars.length; i++) {
            if(chars[i]==searchKey){
                count++;
            }
            if(count==n){
                position=i;
                break;
            }
        }
        if(count<n){
            return string.length();
        }
        return position;
    }
}
