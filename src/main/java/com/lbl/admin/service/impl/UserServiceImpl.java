package com.lbl.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lbl.common.entity.Role;
import com.lbl.common.entity.User;
import com.lbl.common.entity.UserRole;
import com.lbl.common.mapper.RoleMapper;
import com.lbl.common.mapper.UserMapper;
import com.lbl.common.mapper.UserRoleMapper;
import com.lbl.admin.service.PermissionService;
import com.lbl.admin.service.UserService;
import com.lbl.admin.vo.PermissionVo;
import com.lbl.admin.vo.RoleVo;
import com.lbl.admin.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author  lbl
 * @date  2020/5/13 13:12
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Autowired
    private PermissionService permissionService;

    @Override
    public UserVo findUserByUserName(String username) {
        UserVo vo=new UserVo();

        //查询账号信息
        Example example=new Example(User.class);
        example.createCriteria().andEqualTo("username",username);
        User dbUser = userMapper.selectOneByExample(example);

        //拷贝vo对象
        BeanUtil.copyProperties(dbUser,vo);
        //查询角色信息
        List<Role> roles = this.findRolesByUserId(dbUser.getId());

        //转换vo对象
        List<RoleVo> roleVo = roles.stream()
                .map(e -> RoleVo.builder().id(e.getId()).name(e.getName()).parentId(e.getParentId()).build())
                .collect(Collectors.toList());

        vo.setRoles(roleVo);

        //获取菜单
        List<PermissionVo> menus = permissionService.queryTheCurrentUserSMenu();
        vo.setMenus(menus);
        return vo;
    }

    @Override
    public boolean isAccountRegister(String username) {
        //查询账号信息
        Example example=new Example(User.class);
        example.createCriteria().andEqualTo("username",username);
        User dbUser = userMapper.selectOneByExample(example);
        return dbUser!=null;
    }

    @Override
    public int updateBatch(List<User> list) {
        return userMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<User> list) {
        return userMapper.batchInsert(list);
    }

    @Override
    public int insertOrUpdate(User record) {
        return userMapper.insertOrUpdate(record);
    }

    @Override
    public int insertOrUpdateSelective(User record) {
        return userMapper.insertOrUpdateSelective(record);
    }

    @Override
    public List<Role> findRolesByUserId(Long userId) {
        //查询角色信息
        return roleMapper.findRoleByUserId(userId);
    }

    @Override
    public void assigningRolesByUserId(Long userId, List<Long> roles) {
        //先删原来的角色
        Example delExample=new Example(UserRole.class);
        delExample.createCriteria().andEqualTo("userId",userId);
        userRoleMapper.deleteByExample(delExample);
        //再插入新分配的角色
        for (Long roleId : roles) {
            UserRole role=new UserRole();
            role.setRoleId(roleId);
            role.setUserId(userId);
            userRoleMapper.insert(role);
        }
    }

    /**
     * 返回全部记录
     * @return
     */
    public List<User> findAll() {
        return userMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageInfo<User> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        PageInfo<User> pageInfo=new PageInfo<>(userMapper.selectAll());
        return pageInfo;
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<User> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return userMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageInfo<User> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        PageInfo<User> pageInfo=new PageInfo<>(userMapper.selectByExample(example));
        return pageInfo;
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public User findById(Long id) {
        return userMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param user
     */
    public void add(User user) {
        userMapper.insert(user);
    }

    /**
     * 修改
     * @param user
     */
    public void update(User user) {
        userMapper.updateByPrimaryKeySelective(user);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(Long id) {
        userMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<User> findFillInUser(int size,List<String> usernames) {
        return userMapper.findFillInUser(size,usernames);
    }

    @Override
    public User findUserInfoByUsername(String username) {
        Example example=new Example(User.class);
        example.createCriteria().andEqualTo("username",username);
        return userMapper.selectOneByExample(example);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 用户名
            if(searchMap.get("username")!=null && !"".equals(searchMap.get("username"))){
                criteria.andLike("username","%"+searchMap.get("username")+"%");
            }
            // 密码，加密存储
            if(searchMap.get("password")!=null && !"".equals(searchMap.get("password"))){
                criteria.andLike("password","%"+searchMap.get("password")+"%");
            }
            // 头像
            if(searchMap.get("avatar")!=null && !"".equals(searchMap.get("avatar"))){
                criteria.andLike("avatar","%"+searchMap.get("avatar")+"%");
            }
            // 注册手机号
            if(searchMap.get("phone")!=null && !"".equals(searchMap.get("phone"))){
                criteria.andLike("phone","%"+searchMap.get("phone")+"%");
            }
            // 注册邮箱
            if(searchMap.get("email")!=null && !"".equals(searchMap.get("email"))){
                criteria.andLike("email","%"+searchMap.get("email")+"%");
            }
            // 简介
            if(searchMap.get("introduction")!=null && !"".equals(searchMap.get("introduction"))){
                criteria.andLike("introduction","%"+searchMap.get("introduction")+"%");
            }
            // 名字
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }
            // 更新时间
            if(searchMap.get("updated")!=null ){
                criteria.andGreaterThanOrEqualTo("updated", DateUtil.beginOfDay(DateUtil.parse(String.valueOf(searchMap.get("updated")))));
            }

        }
        return example;
    }

}

