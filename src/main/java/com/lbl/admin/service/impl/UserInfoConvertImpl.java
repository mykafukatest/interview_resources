package com.lbl.admin.service.impl;

import com.lbl.admin.security.UserDetailsVo;
import com.lbl.admin.service.UserInfoConvert;
import com.lbl.admin.vo.UserDTO;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * @author lbl
 * @date 2020/6/7 10:58
 */
@Service
public class UserInfoConvertImpl implements UserInfoConvert {

    @Override
    public UserDTO jwtUserToUserDTO() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsVo detailsVo=(UserDetailsVo)authentication.getPrincipal();
        return new UserDTO(detailsVo.getUsername(),detailsVo.getName());
    }
}
