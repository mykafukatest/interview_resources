package com.lbl.admin.service.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lbl.common.entity.Permission;
import com.lbl.common.mapper.PermissionMapper;
import com.lbl.admin.service.PermissionService;
import com.lbl.common.utils.TreeUtils;
import com.lbl.admin.vo.PermissionVo;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author  lbl
 * @date  2020/5/13 13:22
 */
@Service
public class PermissionServiceImpl implements PermissionService {

    @Resource
    private PermissionMapper permissionMapper;

    @Override
    public int updateBatch(List<Permission> list) {
        return permissionMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<Permission> list) {
        return permissionMapper.batchInsert(list);
    }

    @Override
    public int insertOrUpdate(Permission record) {
        return permissionMapper.insertOrUpdate(record);
    }

    @Override
    public int insertOrUpdateSelective(Permission record) {
        return permissionMapper.insertOrUpdateSelective(record);
    }

    @Override
    public List<PermissionVo> queryTheCurrentUserSMenu() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<Permission> permission = permissionMapper.findPermissionByUserName(authentication.getName(),2);
        return TreeUtils.treeMenuConversion(permission);
    }
    /**
     * 返回全部记录
     * @return
     */
    public List<Permission> findAll() {
        return permissionMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageInfo<Permission> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        PageInfo<Permission> pageInfo=new PageInfo<>(permissionMapper.selectAll());
        return pageInfo;
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Permission> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return permissionMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageInfo<Permission> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        return new PageInfo<>(permissionMapper.selectByExample(example));
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Permission findById(Long id) {
        return permissionMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param permission
     */
    public void add(Permission permission) {
        permission.setCreated(new Date());
        permission.setUpdated(new Date());
        permissionMapper.insert(permission);
    }

    /**
     * 修改
     * @param permission
     */
    public void update(Permission permission) {
        permission.setUpdated(new Date());
        permissionMapper.updateByPrimaryKeySelective(permission);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(Long id) {
        permissionMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Permission.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){

            // 主键查询
            if(searchMap.get("id")!=null){
                criteria.andEqualTo("id",searchMap.get("id"));
            }
            // 主键查询
            if(searchMap.get("parentId")!=null){
                criteria.andEqualTo("parentId",searchMap.get("parentId"));
            }
            // 类型查询
            if(searchMap.get("type")!=null){
                criteria.andEqualTo("type",searchMap.get("type"));
            }
            // 权限名称
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }
            // 权限英文名称
            if(searchMap.get("enname")!=null && !"".equals(searchMap.get("enname"))){
                criteria.andLike("enname","%"+searchMap.get("enname")+"%");
            }
            // 授权路径
            if(searchMap.get("url")!=null){
                criteria.andEqualTo("url",searchMap.get("url"));
            }
            // 备注
            if(searchMap.get("description")!=null && !"".equals(searchMap.get("description"))){
                criteria.andLike("description","%"+searchMap.get("description")+"%");
            }
            // 图标
            if(searchMap.get("ico")!=null && !"".equals(searchMap.get("ico"))){
                criteria.andLike("ico","%"+searchMap.get("ico")+"%");
            }
            // 更新时间
            if(searchMap.get("updated")!=null ){
                criteria.andGreaterThanOrEqualTo("updated", DateUtil.beginOfDay(DateUtil.parse(String.valueOf(searchMap.get("updated")))));
            }

        }
        return example;
    }

}
