package com.lbl.admin.service.impl;
import com.github.pagehelper.PageInfo;
import com.lbl.common.mapper.UserRoleMapper;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.lbl.common.entity.UserRole;
import com.lbl.admin.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<UserRole> findAll() {
        return userRoleMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageInfo<UserRole> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        PageInfo<UserRole> pageInfo=new PageInfo<>(userRoleMapper.selectAll());
        return pageInfo;
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<UserRole> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return userRoleMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageInfo<UserRole> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        PageInfo<UserRole> pageInfo=new PageInfo<>(userRoleMapper.selectByExample(example));
        return pageInfo;
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public UserRole findById(Long id) {
        return userRoleMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param userRole
     */
    public void add(UserRole userRole) {
        userRoleMapper.insert(userRole);
    }

    /**
     * 修改
     * @param userRole
     */
    public void update(UserRole userRole) {
        userRoleMapper.updateByPrimaryKeySelective(userRole);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(Long id) {
        userRoleMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(UserRole.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){


        }
        return example;
    }

}
