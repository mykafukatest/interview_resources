package com.lbl.admin.service;


import com.lbl.admin.vo.UserDTO;

/**
 * @author : 汤北寒
 * @date : 2020-05-2020/5/20-20:37
 */
public interface UserInfoConvert {
    /**
     * 此方法自定义session中的用户转换到UserDTO username是账号,nickname是名字
     *
     * @return UserDTO
     */
    default UserDTO jwtUserToUserDTO(){
        UserDTO userDTO = new UserDTO();

        userDTO.setNickname("admin");
        userDTO.setUsername("admin");
        return userDTO;
    }
}
