package com.lbl.admin.service;
import com.github.pagehelper.PageInfo;
import com.lbl.common.entity.RolePermission;

import java.util.*;

/**
 * rolePermission业务逻辑层
 */
public interface RolePermissionService {


    public List<RolePermission> findAll();


    public PageInfo<RolePermission> findPage(int page, int size);


    public List<RolePermission> findList(Map<String, Object> searchMap);


    public PageInfo<RolePermission> findPage(Map<String, Object> searchMap, int page, int size);


    public RolePermission findById(Long id);

    public void add(RolePermission rolePermission);


    public void update(RolePermission rolePermission);


    public void delete(Long id);

}
