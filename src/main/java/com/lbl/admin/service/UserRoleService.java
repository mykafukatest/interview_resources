package com.lbl.admin.service;
import com.github.pagehelper.PageInfo;
import com.lbl.common.entity.UserRole;

import java.util.*;

/**
 * userRole业务逻辑层
 */
public interface UserRoleService {


    public List<UserRole> findAll();


    public PageInfo<UserRole> findPage(int page, int size);


    public List<UserRole> findList(Map<String, Object> searchMap);


    public PageInfo<UserRole> findPage(Map<String, Object> searchMap, int page, int size);


    public UserRole findById(Long id);

    public void add(UserRole userRole);


    public void update(UserRole userRole);


    public void delete(Long id);

}
