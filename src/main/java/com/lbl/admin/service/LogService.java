package com.lbl.admin.service;
import com.github.pagehelper.PageInfo;
import com.lbl.common.entity.Log;

import java.util.*;

/**
 * log业务逻辑层
 */
public interface LogService {


    public List<Log> findAll();


    public PageInfo<Log> findPage(int page, int size);


    public List<Log> findList(Map<String,Object> searchMap);


    public PageInfo<Log> findPage(Map<String,Object> searchMap,int page, int size);


    public Log findById(Integer id);

    public void add(Log log);


    public void update(Log log);


    public void delete(Integer id);

}
