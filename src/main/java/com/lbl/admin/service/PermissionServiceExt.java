package com.lbl.admin.service;

import com.lbl.common.entity.Permission;
import com.lbl.admin.vo.PermissionVo;

import java.util.List;

/**
 * @author  lbl
 * @date  2020/5/13 13:22
 */
public interface PermissionServiceExt {


    int updateBatch(List<Permission> list);

    int batchInsert(List<Permission> list);

    int insertOrUpdate(Permission record);

    int insertOrUpdateSelective(Permission record);

    List<PermissionVo> queryTheCurrentUserSMenu();
}
