package com.lbl.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lbl
 * @date 2020/5/13 16:23
 */
@RestController
@Api(tags = "测试权限接口包含：(SystemUser)")
@RequestMapping("/index")
public class IndexController {
    @PreAuthorize("hasAuthority('SystemUser')")
    @GetMapping("/test1")
    @ApiOperation("包含：(SystemUser)")
    public String test1(){
        return "test1";
    }
}
