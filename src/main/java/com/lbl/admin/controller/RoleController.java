package com.lbl.admin.controller;

import com.github.pagehelper.PageInfo;
import com.lbl.common.annotation.SystemLog;
import com.lbl.common.utils.ResponseResult;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import com.lbl.common.utils.PageResult;
import com.lbl.common.entity.Role;
import com.lbl.admin.service.RoleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/role")
@Api(tags = "角色模块")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/findAll")
    public ResponseResult findAll(){
        return ResponseResult.success(roleService.findAll());
    }

    @GetMapping("/findPage")
    public ResponseResult findPage(int page, int size){
        PageInfo<Role> info = roleService.findPage(page, size);
        return ResponseResult.success(new PageResult<>(info.getTotal(),info.getList()));
    }

    @PostMapping("/search")
    public ResponseResult findList(@RequestBody Map<String,Object> searchMap){
        return ResponseResult.success(roleService.findList(searchMap));
    }


    @PostMapping("/search/{page}/{size}")
    @PreAuthorize("hasAuthority('ROLE_QUERY')")
    public ResponseResult findPage(@RequestBody Map<String,Object> searchMap,
                                             @PathVariable("page") int page,
                                             @PathVariable("size") int size){
        PageInfo<Role> info =roleService.findPage(searchMap,page,size);
        return ResponseResult.success(new PageResult<>(info.getTotal(),info.getList()));
    }

    @GetMapping("/findById/{id}")
    public ResponseResult findById(@PathVariable("id") Long id){
        return ResponseResult.success(roleService.findById(id));
    }

    @SystemLog(module = "角色模块",method = "添加角色")
    @PostMapping("/save")
    @PreAuthorize("hasAuthority('ROLE_SAVE')")
    public ResponseResult add(@RequestBody Role role){
        role.setCreated(new Date());
        role.setUpdated(new Date());
        roleService.add(role);
        return ResponseResult.success();
    }
    @SystemLog(module = "角色模块",method = "更新角色")
    @PreAuthorize("hasAuthority('ROLE_UPDATE')")
    @PostMapping("/update")
    public ResponseResult update(@RequestBody Role role){
        role.setUpdated(new Date());
        roleService.update(role);
        return ResponseResult.success();
    }
    @GetMapping("/delete/{id}")
    @SystemLog(module = "角色模块",method = "删除角色")
    @PreAuthorize("hasAuthority('ROLE_DELETE')")
    public ResponseResult delete(@PathVariable("id")Long id){
        roleService.delete(id);
        return ResponseResult.success();
    }

    @GetMapping("/querySelectByRoleId/{roleId}")
    public ResponseResult querySelectByRoleId(@PathVariable("roleId")Long roleId){
        Map<String, Object> result = roleService.queryMenuBasedOnRoleId(roleId);
        return ResponseResult.success(result);
    }
    @SystemLog(module = "角色模块",method = "分配权限")
    @GetMapping("/addAuthorityByRoleId")
    @PreAuthorize("hasAuthority('ROLE_ADDAUTHORITYBYROLEID')")
    public ResponseResult addAuthorityByRoleId(@RequestParam("roleId")Long roleId, @RequestParam("menuList")List<Long> menuList){
        roleService.addAuthorityByRoleId(roleId,menuList);
        return ResponseResult.success();
    }

}
