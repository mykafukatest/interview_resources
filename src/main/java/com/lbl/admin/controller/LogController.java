package com.lbl.admin.controller;

import com.github.pagehelper.PageInfo;
import com.lbl.common.annotation.SystemLog;
import com.lbl.common.entity.Log;
import org.springframework.beans.factory.annotation.Autowired;
import com.lbl.common.utils.PageResult;
import com.lbl.common.utils.ResponseResult;
import com.lbl.admin.service.LogService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping("/log")
public class LogController {

    @Autowired
    private LogService logService;

    @GetMapping("/findAll")
    public ResponseResult findAll(){
        return ResponseResult.success(logService.findAll());
    }

    @GetMapping("/findPage")
    public ResponseResult findPage(int page, int size){
        PageInfo<Log> info = logService.findPage(page, size);
        return ResponseResult.success(new PageResult<>(info.getTotal(),info.getList()));
    }

    @PostMapping("/search")
    public ResponseResult findList(@RequestBody Map<String,Object> searchMap){
        return ResponseResult.success(logService.findList(searchMap));
    }


    @PostMapping("/search/{page}/{size}")
    @PreAuthorize("hasAuthority('LOG_QUERY')")
    public ResponseResult findPage(@RequestBody Map<String,Object> searchMap,
                                             @PathVariable("page") int page,
                                             @PathVariable("size") int size){
        PageInfo<Log> info =logService.findPage(searchMap,page,size);
        return ResponseResult.success(new PageResult<>(info.getTotal(),info.getList()));
    }

    @GetMapping("/findById/{id}")
    public ResponseResult findById(@PathVariable("id") Integer id){
        return ResponseResult.success(logService.findById(id));
    }


    @PostMapping("/save")
    @SystemLog(module = "日志模块保存日志",method = "add")
    @PreAuthorize("hasAuthority('LOG_ADD')")
    public ResponseResult add(@RequestBody Log log){
        logService.add(log);
        return ResponseResult.success();
    }

    @PostMapping("/update")
    @SystemLog(module = "日志模块修改日志",method = "update")
    @PreAuthorize("hasAuthority('LOG_UPDATE')")
    public ResponseResult update(@RequestBody Log log){
        logService.update(log);
        return ResponseResult.success();
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('LOG_DELETE')")
    public ResponseResult delete(@PathVariable("id")Integer id){
        logService.delete(id);
        return ResponseResult.success();
    }

}
