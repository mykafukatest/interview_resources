package com.lbl.admin.controller;

import com.github.pagehelper.PageInfo;
import com.lbl.common.annotation.SystemLog;
import com.lbl.common.entity.Permission;
import com.lbl.admin.service.PermissionService;
import com.lbl.common.utils.PageResult;
import com.lbl.common.utils.ResponseResult;
import com.lbl.common.utils.TreeUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@Api(tags = "权限模块")
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @GetMapping("/findAll")
    public ResponseResult findAll(){
        return ResponseResult.success(permissionService.findAll());
    }

    @GetMapping("/treeFindAll")
    public ResponseResult treeFindAll(){
        return ResponseResult.success(TreeUtils.treeMenuConversion(permissionService.findAll()));
    }

    @GetMapping("/findPage")
    public ResponseResult findPage(int page, int size){
        PageInfo<Permission> info = permissionService.findPage(page, size);
        return ResponseResult.success(new PageResult<>(info.getTotal(),info.getList()));
    }

    @PostMapping("/search")
    public ResponseResult findList(@RequestBody Map<String,Object> searchMap){
        return ResponseResult.success(permissionService.findList(searchMap));
    }

    @PostMapping("/search/{page}/{size}")
    @PreAuthorize("hasAuthority('MENU_QUERY')")
    public ResponseResult findPage(@RequestBody Map<String,Object> searchMap,
                                             @PathVariable("page") int page,
                                             @PathVariable("size") int size){
        PageInfo<Permission> info = permissionService.findPage(searchMap, page, size);
        return ResponseResult.success(new PageResult<>(info.getTotal(),info.getList()));
    }

    @GetMapping("/findById/{id}")
    public ResponseResult findById(@PathVariable("id") Long id){
        return ResponseResult.success(permissionService.findById(id));
    }


    @PostMapping("/save")
    @PreAuthorize("hasAuthority('MENU_ADD')")
    @SystemLog(module = "权限模块",method = "add")
    public ResponseResult add(@RequestBody Permission permission){
        permissionService.add(permission);
        return ResponseResult.success();
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('MENU_EDIT')")
    @SystemLog(module = "权限模块",method = "update")
    public ResponseResult update(@RequestBody Permission permission){
        permissionService.update(permission);
        return ResponseResult.success();
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('MENU_DELETE')")
    @SystemLog(module = "权限模块",method = "delete")
    public ResponseResult delete(@PathVariable("id") Long id){
        permissionService.delete(id);
        return ResponseResult.success();
    }

}
