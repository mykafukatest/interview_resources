package com.lbl.admin.test;

import java.io.*;
import java.util.Stack;

/**
 * @author lbl
 * @date 2020/7/5 17:44
 */
public class TestQuestions {

    public static void main(String[] Args) throws IOException {
        FileInputStream fileInputStream=new FileInputStream("G:\\git\\lbl\\template\\src\\main\\java\\com\\lbl\\template\\test\\TestReversion.txt");
        BufferedReader br=new BufferedReader(new InputStreamReader(fileInputStream));
        Stack<String> stack=new Stack<>();
        String line=null;
        while ((line=br.readLine())!=null){
            String[] split = line.split(" ");
            for (String s : split) {
                stack.push(s);
            }
        }
        while (!stack.empty()){
            System.out.print(stack.pop()+" ");
        }
    }
}
