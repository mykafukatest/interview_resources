package com.lbl.admin.test;

import org.springframework.stereotype.Component;


/**
 * @author lbl
 * @date 2020/7/7 17:26
 */
@Component
public class SpringTest {
    public static void main(String[] args) {
        int a[]={1,2,3};
        String b[]={"A","B","C"};
        String c[]={"D","E","F"};
        Object o1=new Object();
        Object o2=new Object();
        Object o3=new Object();

        new Thread(()->{
            for (int i = 0; i < a.length; i++) {
                synchronized (o1){
                    synchronized (o2){
                        System.out.println(a[i]);
                        try {
                            o2.notifyAll();
                            o2.wait();
                            o1.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }).start();
        Thread thread3 = new Thread(() -> {
            for (int i = 0; i < c.length; i++) {
                synchronized (o3) {
                    System.out.println(c[i]);
                    o1.notifyAll();
                    try {
                        o3.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        new Thread(()->{
            for (int i = 0; i <b.length; i++) {
                synchronized (o2){
                    synchronized (o3){
                        System.out.println(b[i]);
                        thread3.start();
                        try {
                            o3.notifyAll();
                            o3.wait();
                            synchronized (o1){


                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();

    }
}
