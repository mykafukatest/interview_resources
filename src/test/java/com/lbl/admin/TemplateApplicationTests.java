package com.lbl.admin;

import com.lbl.common.mapper.UserMapper;
import com.lbl.common.utils.FastDFSClientUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class TemplateApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private FastDFSClientUtil fastDFSClientUtil;


    @Test
    void contextLoads() throws IOException {
        String s = fastDFSClientUtil.uploadFile(new File("C:\\Users\\feng\\Desktop\\java.png"));
        System.out.println(s);
    }
    @Test
    void setbCryptPasswordEncoder() throws IOException {
        List<String> a1 = Arrays.asList("1", "2", "3");
        List<String> a2 = Arrays.asList("a", "b", "c");
        List<List<String>> all=new ArrayList<>();
        all.add(a1);
        all.add(a2);

        String collect = all.stream().flatMap(e -> e.stream()).collect(Collectors.joining(";"));
        System.out.println(collect);
    }

}
